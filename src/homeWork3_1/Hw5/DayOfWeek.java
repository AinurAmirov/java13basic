package homeWork3_1.Hw5;

import java.util.Scanner;

public class DayOfWeek {
    private byte day;
    private String nameOfWeek;
    private DayOfWeek[] namesOfWeek;
    Scanner scanner = new Scanner(System.in);
    public DayOfWeek(String newNameOfWeek){
        this.nameOfWeek = newNameOfWeek;
    }

    public DayOfWeek(byte newDay){
        this.day = newDay;
        namesOfWeek = new DayOfWeek[newDay];
        for (int i = 0; i < newDay; i++) {
            String newNameOfWeek = scanner.next();
            namesOfWeek[i] = new DayOfWeek(newNameOfWeek);
        }
    }

    public String getNameOfWeek(){
        return this.nameOfWeek;
    }

    public byte getDay(){
        return this.day;
    }
    public void getDayOfWeek(){
        for (int i = 0; i < getDay(); i++) {
            System.out.println((i + 1) + " " + namesOfWeek[i].getNameOfWeek());
        }
    }
}