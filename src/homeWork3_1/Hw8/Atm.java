package homeWork3_1.Hw8;

public class Atm {
    private static double RUB_PER_DOL;
    private static double DOL_PER_RUB;
    private static int n;

    public Atm(double rub_per_dol) {
        this.RUB_PER_DOL = rub_per_dol;
        this.DOL_PER_RUB = 1 / rub_per_dol;
        n++;
    }

    public double getDOL_PER_RUB() {
        return DOL_PER_RUB;
    }

    public double getRUB_PER_DOL() {
        return RUB_PER_DOL;
    }

    public void AtmRub(int rub){
        System.out.println(rub * RUB_PER_DOL);
    }

    public void AtmDol(int dol){
        System.out.println(dol * DOL_PER_RUB);
    }

    public int getN(){
        return n;
    }
}
