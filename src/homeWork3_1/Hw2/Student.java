package homeWork3_1.Hw2;

import java.util.Scanner;

public class Student {
    private String name;
    private String surname;
    private int[] grades;
    private double x;

    static Scanner scanner = new Scanner(System.in);

//    public Student(String newName, String newSurname, int newX){
//        this.name = newName;
//        this.surname = newSurname;
//        this.x = newX;
//    }

    public Student(String newName, String newSurname, int newGrades){
        this.name = newName;
        this.surname = newSurname;
        Grades(newGrades);
    }

    public Student(){
    }
    public Student(String newName, String newSurname) {
        this.name = newName;
        this.surname = newSurname;
    }

    public String getName() {
        return name;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String newSurname) {
        this.surname = newSurname;
    }

//присваивает оцинки в массив через ввод, если оценок меньше 10, то им присваивается 0
    public void Grades(int n) {
        if (n > 10) {
            System.out.println("Введите последние 10 оценок");
            grades = new int[10];
            for (int i = 0; i < 10; i++) {
                grades[i] = scanner.nextInt();
            }
        } else {
            System.out.println("Введите оценки по порядку");
            grades = new int[10];
            for (int i = 0; i < n; i++) {
                grades[i] = scanner.nextInt();
            }
        }
    }

    //возвращает оцинки в виде строки через пробел
        public String getGrades () {
            String g = "";
            for (int i = 0; i < grades.length; i++) {
                g += grades[i] + " ";
            }
            return g;
        }

        //высчитывает количество не нулей в массиве, от чего либо добавляет оценку, либо смещает их
        public void setGrades (int n) {
            int sum = 0;
            for (int i = 0; i < 10; i++) {
                if (grades[i] != 0) {
                    sum += 1;
                }
            }
            if (sum == 10) {
                for (int i = 1; i < 9; i++) {
                    grades[i - 1] = grades[i];
                }
                grades[8] = grades[9];
                grades[9] = n;
            } else if (sum < 10) {
                grades[sum] = n;
            }
        }

        //вычисляет среднюю оценку
        double x() {
            int sum = 0;
            for (int i = 0; i < 10; i++) {
                sum += grades[i];
            }
            x = ((double) sum / 10);
            return this.x;
        }

        public double getX(){
        return this.x();
        }

        public void setX(double newX){
        this.x = newX;
        }
}



