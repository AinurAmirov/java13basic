package homeWork3_1.Hw4;

import com.sun.source.tree.BreakTree;

public class TimeUnit {
    private int time;
    private int minute;
    private int seconds;

    public TimeUnit(int newTime, int newMinute, int newSeconds) {
        if(newTime >= 0 && newTime <= 24 && newMinute >= 00 && newMinute < 60 && newSeconds >=00 && newSeconds < 60) {
            this.time = newTime;
            this.minute = newMinute;
            this.seconds = newSeconds;
        }
    }

    public TimeUnit(int newTime, int newMinute){
        this.time = newTime;
        this.minute = newMinute;
        this.seconds = 00;
    }

    public TimeUnit(int newTime){
        this.time = newTime;
        this.minute = 00;
        this.seconds = 00;
    }

    public void thisTime(){
        System.out.println(time + ":" + minute + ":" + seconds);
    }

    public void ampmTime(){
        if(time > 12) {
            System.out.println((time - 12) + ":" + minute + ":" + seconds + " pm");
        } else if(time < 12){
            System.out.println(time + ":" + minute + ":" + seconds + " am");
        } else if(time == 12){
            System.out.println("00" + ":" + minute + ":" + seconds + " pm");
        } else if(time == 24){
            System.out.println("00" + ":" + minute + ":" + seconds + " am");
        }
    }

    public String arratTime(int newTime, int newMinute, int newSeconds){
        if(time + newTime < 24) {
            this.time = time + newTime;
        } else {
            this.time = time + newTime - 24;
        }

        if(minute + newMinute < 60) {
            this.minute = minute + newMinute;
        } else {
            this.minute = minute + newMinute - 60;
        }

        if(seconds + newSeconds < 60) {
            this.seconds = seconds + newSeconds;
        } else {
            this.seconds = seconds + newSeconds - 60;
        }

        return time + ":" + minute + ":" + seconds;
    }
}
