package homeWork3_1.Hw1;

public class Cat {

    private String sleep(){
        return "Sleep";
    }

    private String meow(){
        return "Meow";
    }

    private String eat(){
        return "Eat";
    }

    public static String status(){
        int x = (int) (Math.random() * 3 + 1);
        Cat e = new Cat();
        if (x == 1) {
            return e.sleep();
        } else if (x == 2) {
            return e.meow();
        } else {
            return e.eat();
        }
    }

    public static void main(String[] args) {
//        Cat cat = new Cat();
        System.out.println(Cat.status());
    }
}
