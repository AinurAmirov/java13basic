package homeWork3_1.Hw7;

public class TriangleChecker {
    private double a;
    private double b;
    private double c;

    public TriangleChecker(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
        boolean flag = false;
        if (a + b > c) {
            if (a + c > b) {
                if (c + b > a)
                    flag = true;
            }
        }
        System.out.println(flag);
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }
}
