package homeWork3_1.hw6;

public class AmazingString {
    private String arrayString;
    private char[] arrayChar;

    public AmazingString(char[] arrayChar1){
        this.arrayChar = arrayChar1;
    }

    public AmazingString(String arrayString){
        this.arrayString = arrayString;
        char [] char1 = new char[arrayString.length()];
        for (int i = 0; i < arrayString.length(); i++) {
            char1[i] = arrayString.charAt(i);
        }
        this.arrayChar = char1;
    }

    public void setArrayChar(char[] arrayChar) {
        this.arrayChar = arrayChar;
    }

    public void setArrayString(String arrayString) {
        this.arrayString = arrayString;
    }

    public String getArrayString() {
        return arrayString;
    }

    public char[] getArrayChar() {
        return arrayChar;
    }

    public char arraySimbol(int n){
        return getArrayChar()[n];
    }

    public char arraySimbolString(int n){
            return getArrayString().charAt(n);
    }

    public int sinbolLenght(){
        int sum = 0;
        for (int i = 0; i < getArrayChar().length; i++) {
            sum += 1;
        }
        return sum;
    }

    public char[] returnString(){
        return getArrayChar();
    }

    public boolean comTo(char[] arrayChar){
        boolean flag = false;
        for (int i = 0; i < getArrayChar().length; i++) {
            for (int j = 0; j < arrayChar.length; j++) {
//                int k = 0;
//                if(getArrayChar()[i + k] == arrayChar[j + k]){
                if(getArrayChar()[i] == arrayChar[j]){
                    for (int l = 0; l < arrayChar.length - 1; l++) {
                        if(getArrayChar()[i + l] == arrayChar[j + l]){
                            flag = true;
                        } else
                            flag = false;
                        break;
                    }
//                    k++;
//                    flag = true;
                }
//                } else
//                    flag = false;
//                break;
            }
        }
        return flag;
    }
}
