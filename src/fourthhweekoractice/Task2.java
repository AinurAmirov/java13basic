package fourthhweekoractice;

import java.util.Scanner;

/*
Даны два числа m и n
Найти произведение чисел в диапазоне m и n включительно
(m < 14 n < 14), m < n
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();

        int res = 1;
        for (int i = m; i <= n; i++) {
            res *= i;
            System.out.println(res);
        }
    }
}
