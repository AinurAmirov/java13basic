package fourthhweekoractice;

import java.util.Scanner;

/*
Дано целое число н (больше 0)
Вывести сумму всех цифр этого число

 */
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        String n = scanner.nextLine();
//        int sum = 0;
//        for (int i = 1;  i <= n.length(); i++)
//            sum += Integer.parseInt(String.valueOf(n.charAt(i))); ???
        int n = scanner.nextInt();
        int a = 1;
        int sum;
        for (sum = 0; (n / a) > 0; a *= 10) {
            sum += (n / a) % 10;
        }
        System.out.println(sum);
        /*
        int n = scanner.nextInt();
        int sum = 0;
        while (n > 0) {
        sum = sum + n % 10;
        n = n / 10;
        }
         */
        /*
        sum += Integer.parsInt(String.valueOf(num.charAt(i));
         */
    }
}
