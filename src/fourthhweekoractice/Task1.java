package fourthhweekoractice;

import java.util.Scanner;

/*
Дано число n < 13, n > 0
Найти факториал числа n (n! = 1 * 2 * ... * (n - 1) * n)
 */
public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int res = 1;
        int i;
        for (i = 2; i <= n; i++) {
            res *= i;
            System.out.println(res);
        }
        System.out.println(res);

    }
}
