package fourthhweekoractice;

import java.awt.desktop.ScreenSleepEvent;
import java.util.Scanner;

/*
Даны числа m < 13, n < 7
Вывести все степени (от 0 до н включительно) числа м с помощью  цикла
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();

        for (int i = 0; i <= n; i++) {
            System.out.println((int) Math.pow(m, i));

            /*
            int res = 1;
            for (int i = 1; i <= n; i++) {
            res *= m;
            System.out.println(res);
             */

            /*
            int res = 1;
            int i = 0;
            while (i <= n) {
            res *= m;
            System.out.println(res);
            i++;
            }
             */
        }
    }
}
