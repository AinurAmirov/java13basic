package fourthhweekoractice;

import java.util.Scanner;

/*
Дано целое число n
Найти n число Фибоначчи с помощью цикла

//0, 1, 1, 2, 3, 5, 8, 13 ... 109646
F0 = 0
F1 = 1
Fn = Fn-1 + Fn-2, n >= 2
 */
public class task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int f0 = 0;
        int f1 = 1;
        if (n == 0) {
            System.out.println(f0);
            System.exit(0);
        } if (n == 1) {
            System.out.println(f1);
            return;
        }
        int fib = 0;
        for (int i = 2; i <= n;  i++) {
            fib = f0 + f1;
            f0 = f1;
            f1 = fib;
            System.out.println(fib);
        }

    }
}
