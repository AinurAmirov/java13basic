package fourthhweekoractice;

import java.util.Scanner;

/*
На вход подается число н и последовательность целых чисел длины н
Ввывести два максимальных числа в этой последовательности без использования массивов
5
1 3 5 4 5 -> 5 5
 */
public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int fn = scanner.nextInt();
        int sn = scanner.nextInt();

        int firstMax = Math.max(fn, sn);
        int secondMax = Math.min (fn, sn);

        for (int i = 2; i < n; i++) {
            int k = scanner.nextInt();
            if (k > firstMax) {
                secondMax = firstMax;
                firstMax = k;
            } else if (k > secondMax) {
                secondMax = k;
            }

        }
        System.out.println(firstMax + " " + secondMax);

    }
}
