package fourthhweekoractice;

import java.util.Scanner;

/*
Начальный вклад в банке равен 1000
Каждый месяц размер вклада увеличивается на Р процентов от имеющейся суммы (0 < P < 25)
Найти через какое кол-во времени размер вклада будет больше 1100
и вывести найденное кол-во месяцев и вывести итоговый размер вклада
 */
public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int p = scanner.nextInt();
        int res = 0;
        double sum;
        for (sum = 1000; sum <= 1100; res++) {
            double a = sum * p / 100.0;
            sum = sum + a;
        }
        System.out.println(sum);
        System.out.println(res);
    }
}
