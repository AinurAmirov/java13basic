package fourthhweekoractice;

import java.util.Scanner;

/*
Дана последовательность из Н целых чисел, которая может начинаться с отрицательного числа
Определить какое кол-во  отрицательныз чисел записано в  начале последовательности и прекратить
выполнение программы при получении первого неотрицательно числа на вход
 */
public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        int res =  0;
//        while (true) {
//            if (scanner.nextInt() < 0) {
//                res++;
//            } else
//                break;
//        }
//        System.out.println(res);

        int count = 0;
        for (int i = scanner.nextInt(); i < 0; i = scanner.nextInt()) {
            count++;
        }
        System.out.println(count);
    }
}
