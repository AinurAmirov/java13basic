package homeWork1_3;

import java.util.Scanner;

public class Hw5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        if (m > n) {
            do {
                m -= n;
            } while (m >= n);
        }
        System.out.println(m);
    }
}
