package homeWork1_3;

import java.util.Scanner;

public class Hw10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 1; i <= n; i++) {
            for (int j = i; j < n; j++) {
                System.out.print(" ");
            }
            for (int j = i; j > 1; j--) {
                System.out.print("#");
            }
            for (int j = 1; j <= i; j++) {
                System.out.print("#");
            }
            System.out.println();
        }
        for (int a = 1; a < n; a++) {
            System.out.print(" ");
            if (a + 1 == n) {
                System.out.print("|");
            }
        }
    }
}


 /*   public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        for (int i = 0; i <= n; i++) {
            int sum = 0;
            if (i == n) {
                for (int e = 0; e < n; e++) {
                    System.out.print("_");
                }
                System.out.println("|");
            } else if (n != 0) {
                for (int a = n - i; a > 0; a--) {
                    System.out.print("_");
                    sum += 1;
                    if (a == 1) {
                        for (int b = 0; b <= 2 * (n - sum); b++) {
                            System.out.print("*");
                            if (b == 2 * (n - sum)) {
                                System.out.println("\n");
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}
*/