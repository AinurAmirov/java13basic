package homeWork1_2;

import java.util.Scanner;

public class Hw12 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String password = scanner.nextLine();

        if (password.matches("^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\\_\\-\\*]).{8,}$")) {
            System.out.println("пароль надежный");
        } else {
            System.out.println("пароль не прошел проверку");
        }
    }
}
