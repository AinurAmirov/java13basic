package homeWork1_2;

import java.util.Scanner;

public class Hw14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String phone = scanner.nextLine();
        int price = scanner.nextInt();

        if ((phone.matches("(iphone)+.*|(samsung)+.*")) && (price >= 50000 && price <= 120000)) {
            System.out.println("Можно купить");
        } else {
            System.out.println("Не подходит");
        }
    }
}
