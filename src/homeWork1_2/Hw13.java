package homeWork1_2;

import java.util.Scanner;

public class Hw13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String a = scanner.nextLine();
        if (a.matches("(камни!).*(запрещенная продукция)"))  {
            System.out.println("в посылке камни и запрещенная продукция");
        } else if (a.matches("(запрещенная продукция).*(камни!)")) {
            System.out.println("в посылке камни и запрещенная продукция");
        } else if (a.matches("(камни!)?.*(камни!).*")) {
            System.out.println("камни в посылке");
        } else if (a.matches("(запрещенная продукция)?.*(запрещенная продукция).*")) {
            System.out.println("в посылке запрещенная продукция");
        } else {
            System.out.println("все ок");
        }

    }
}
