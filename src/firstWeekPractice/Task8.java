package firstWeekPractice;
/*
Дано целое число n.
Выведите следующее за ним четное число.
При решении этой задачи нельзя использовать условную инструкцию if и циклы.

5 -> 6
10 -> 12
 */

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int a = (n / 2 + 1) * 2;
        int a1 = n + 2 - n % 2;

        System.out.println(a);
        System.out.println(a1);
    }
}
