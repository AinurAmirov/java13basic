package firstWeekPractice;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int c = scanner.nextInt();

        System.out.println("Результат:");
        System.out.print("Единицы = " + c % 10 + "; Десятки = " + c / 10);
    }
}
