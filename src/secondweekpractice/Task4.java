package secondweekpractice;

import java.util.Scanner;

public class Task4 {
    public static final int VIP_PRICE = 125;
    public static final int PREMIUM_PRICE = 110;
    public static final int STANDART_PRICE = 110;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int roomType = scanner.nextInt();
//
//        if (roomType == 1) {
//            System.out.println("VIP Price: " +  VIP_PRICE);
//        }
//        if (roomType == 2) {
//            System.out.println("Premium Price: " +  PREMIUM_PRICE);
//        }
//        if (roomType == 3) {
//            System.out.println("Standart Price: " +  STANDART_PRICE);
//        }
//        else if(roomType < 1 && roomType > 3) {
//            System.out.println("don't correct");
//        }
        switch (roomType) {
            case 1:
                System.out.println("VIP Price: " +  VIP_PRICE);
                break;
            case 2:
                System.out.println("Premium Price: " +  PREMIUM_PRICE);
                break;
            case 3:
                System.out.println("Standart Price: " +  STANDART_PRICE);
                break;
            default:
                System.out.println("don't correct");
                break;
//            switch (roomType) {
//                case 1 -> System.out.println("VIP Price: " + VIP_PRICE);
//                case 2 -> System.out.println("Premium Price: " + PREMIUM_PRICE);
//                case 3 -> System.out.println("Standart Price: " + STANDART_PRICE);
//                default -> System.out.println("don't correct");
        }
    }
}
