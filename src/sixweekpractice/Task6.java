package sixweekpractice;

import java.util.Scanner;

/*
На вход подается число Н - ширина  и высота матрицы
необходимо заполнить матрицу 1 и 0 в виде шахматной доски
нулевой элемент должен быть  0
 */
public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int [][] m = new int[n][n];
        int sum = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (sum % 2 == 0)
                    m[i][j] = 1;
                else
                    m[i][j] = 0;
                sum += 1;
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(m[i][j] + " ");
            }
            System.out.println();
        }
    }
}
