package sixweekpractice;

import java.util.Scanner;

/*
написать функцию через рекурсию для вычисления  суммы заданных положительных чисел А и Б
без использования суммы
 */
public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        System.out.println(rek(a, b));
    }

    public static int rek(int a, int b) {
        if (b == 0)
            return a;
        return rek(a + 1, b - 1);
    }
}
