package sixweekpractice;

import java.util.Scanner;

/*
найти факториал Н через рекурсию
 */
public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        /* без рекурсии
        int res = 1;
        for (int i = 1; i <= n; i++) {
            res = res * i;
        }
        System.out.println(res);

         */
        System.out.println(rek(n));
    }
    public static int rek(int i) {
        if (i <= 1)
            return 1;
        else
            return i * rek(i - 1);
    }
}
