package sixweekpractice;

import java.util.Scanner;

/*
на вход подается число Н - высота двумерного массива и  М - его ширина
затем передается сам массив
необходимо сохранить в одномерном массиве суммы чисел каждого столбца и вывести их на экран
 */
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int [][] s = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                s[i][j] = scanner.nextInt();
            }
        }

        int [] a =  new int[m];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                a[i] += s[j][i];
            }
            System.out.println(a[i]);
        }
    }
}
