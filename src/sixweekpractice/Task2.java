package sixweekpractice;

import java.util.Scanner;

/*
на вход подается число Н
проверить является ли оно степерью двойки через рекурсию
вывести тру или фолс
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(rek(n));
    }

    public static boolean rek(int n) {
        if(n == 2 || n == 1)
            return true;
        if (n <= 0 || n % 2 != 0)
            return false;

        return rek(n /  2);
    }
}
