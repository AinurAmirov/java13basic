package sixweekpractice;

import java.util.Scanner;

/*
на вход  подается число  Н - ширина и высота матрицы
Затем передается сама матрица состоящая  из натуральных чисел
после передается число П

Необходимо найти в матрице число  П и занулить строку и столбец в котором это число
находится, кроме числа П
применить ко всем П
 */
public class Task7 {
    public static void main(String[] args) {
        Scanner scanner =  new Scanner(System.in);
        int n = scanner.nextInt();
        int [][] m  = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                m[i][j] = scanner.nextInt();
            }
        }
        int p = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (m[i][j] == p) {
                    fillWithZero(m, n, i, j, p);
                    }
                }
            }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(m[i][j] + " ");
            }
            System.out.println();
        }
        }

    private static void fillWithZero(int[][] m, int n, int i, int j, int p) {
        for (int k = 0; k < n; k++) {
            if (m[k][j] != p) {
                m[k][j] = 0;
            }
            if (m[i][k] != p) {
                m[i][k] = 0;
            }
        }
    }
}
