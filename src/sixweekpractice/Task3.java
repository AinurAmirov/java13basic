package sixweekpractice;

import java.util.Scanner;

/*
развернуть  строку рекурсивно
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String n = scanner.next();
        System.out.println(rek(n));
    }

    public static String rek(String n) {
        if (n.isEmpty())
            return n;
        else {
            System.out.println(n.charAt(0));
            System.out.println(n.substring(1));
            return rek(n.substring(1)) + n.charAt(0);
        }
    }
}
