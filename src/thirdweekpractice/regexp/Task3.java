package thirdweekpractice.regexp;

import java.util.Scanner;

/*
Запросить у пользователя имя, день рождения, номер телефона и емаил
Каждое из полученных ответов проверить регулярным выражением по описанным ниже правилам
Если все введено верно, вывести "ок", если нет - "wrong Answer"
Проверки
имя - длжно содержать только буквы, начинаться с закгавной, а далее прописные
от 2 до 28 символов
день рожденияя - хх.мм.уууу
номер телефона - начинается со знака "+", далее ровно 11 цифр
емеил - в начале буквы или цифры или один из спецсимволом _ - * .
далее - @
далее прописные буквы, далее точка, далее ру или ком
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        String phone = scanner.nextLine();
        String date = scanner.nextLine();
        String email = scanner.nextLine();

        if (
                name.matches("[A-Z][a-z]{1,19}")
                && date.matches("\\d{2}\\.\\d{2}\\.\\d{4}")
                &&phone.matches("\\+\\d{11}")
                &&email.matches("[a-zA-Z0-9\\_\\-\\*\\.]+@[a-z0-9]+\\.(com|ru)")) {
            System.out.println("Ok");
        } else {
            System.out.println("Wrong");
        }
    }
}
