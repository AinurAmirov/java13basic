package thirdweekpractice.regexp;

import java.util.Scanner;

/*
Проверить номер карты и ПИН код
На вход дается две строки: первая содержит номер карты, вторая - ПИН код
Проверить что первая строка состоит 16 цифр,
разделенных провебелом (вид ХХХХ ХХХХ ХХХХ ХХХХ) и ропверить что вторая состоит из 4 цифр
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String cardNumber = input.nextLine();
        String code = input.nextLine();
        System.out.println("Card number in valid:  " + cardNumber.matches("([0-9]{4} ){3}[0-9]{4}"));
        System.out.println("PIN is valid: " + code.matches("[0-9]{4}"));
        }

    }

