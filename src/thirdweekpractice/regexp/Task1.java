package thirdweekpractice.regexp;

import java.util.Scanner;

/*
Проверить является ли введенная строка корректным hex номером цвета.
Коректная строка состоит из 7 символов, первый символ #, далее цифры или буквы от A до F (заглавные или прописные)

Если строка является корректным hex номером цвета, то вывести true, иначе false
 */
public class Task1 {
    public static void main(String[] args) {
        String inputColor = new Scanner(System.in).nextLine();
        System.out.println("Our input data: " + inputColor);

        //первое регулярное выражение на джава
        System.out.println(inputColor.matches("#[a-fA-F0-9]{6}"));
    }
}
