package thirdweekpractice.regexp;

import java.util.Scanner;

/*
Перевод CamelCase B snake_case
На вход подается строка, состаящая из заглавных и прописны латинских букв
вида CamelCase. Ввести эту же строку но состосящую только из прописных букв
вида snake_case, а перед местом, где была заглавная буква - должен быть выведен сивол нижнего подчеркивания
 */
public class Task4 {
    public static void main(String[] args) {
        String inputStr = new Scanner(System.in).nextLine();

        System.out.println("initial value: " + inputStr);
        System.out.println("Updated value: " + inputStr.replaceAll("([a-z])([A-Z]+)",
                "$1_$2").toLowerCase());

    }
}
