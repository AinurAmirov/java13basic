package fifthweekpractice;

import java.util.Scanner;

/*
на вход подается число н - длина массива
затем подается отсортированный  по возрастанию массив целых чисел из Н элементов
после этого число М

найти в массиве все пары чисел,  который в сумме дают число Ь и вывести их на экран
если таких нет, то -1
 */
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        boolean flag = false;

        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }

        int m = scanner.nextInt();

        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (a[i] + a[j] == m) {
                    System.out.println(a[i] + " " + a[j]);
                    flag = true;
                }
            }
        }
        if (!flag)
            System.out.println(-1);
    }
}
