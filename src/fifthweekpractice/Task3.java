package fifthweekpractice;

import java.util.Scanner;

/*
На вход подается число н - длина массива
затем подается массив целых чисел длины н

проверить является ли он отсортированным массивом строго по убыванию
если да, то тру, иначе фолс
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] x = new int[n];

        for (int i = 0; i < n; i++) {
            x[i] = scanner.nextInt();
        }
//        boolean flag = true;
//        int j = 0;
//        for (int i = 1; i < n; i++, j++) {
//            if (x[j] < x[i]) {
//                flag = false;
//                break;
//            }
//            }
//            System.out.println(flag);
        System.out.println(checkIfArrayDesc(x));
        }

    public static boolean checkIfArrayDesc(int[] x) {
        for (int i = 0; i < x.length - 1; i++) {
            if (x[i] < x[i + 1]) {
                return false;
            }
        }
        return true;
    }
    }

