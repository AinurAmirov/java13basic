package fifthweekpractice;

import java.util.Scanner;

/*
на вход подается число н - длина массива
затем подается массив целых чисел длины н
вывести элементы стоящие на четных индексах массива
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int [] x = new int[n];
        for (int i = 0; i < n; i++) {
            x[i] = scanner.nextInt();
        }
        for (int i = 0; i < n; i++) {
            if ((i) % 2 == 0)
                System.out.println(x[i]);

        }

    }
}
