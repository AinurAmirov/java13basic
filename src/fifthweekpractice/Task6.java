package fifthweekpractice;

import java.util.Arrays;
import java.util.Scanner;

/*
На вход подается число Н - длина массива
затем сам массив
Нужно циклически сдвинуть элементы на 1 влево

 */
public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        int temp = a[0];
        for (int i = 0; i < n - 1; i++) {
            a[i] = a[i + 1];
        }
        a[n - 1] = temp;
        System.out.println(Arrays.toString(a));
    }
}
/* альтернативное решение

int first = a[0];
System.arraycopy(a, 1, a, 0, n -1);
a[n - 1] = first;
System.out.println(Arrays.toString(a));

 */