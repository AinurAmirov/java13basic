package fifthweekpractice;

import java.util.Scanner;

/*
На вход подается число н - длина массива
затем подается массив целых чисел длины н
вывести все четные элементы массива
если таких элементов нет, вывести -1
 */
public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int [] x = new int[n];

        for (int i = 0; i < x.length; i++)
            x[i] = scanner.nextInt();

        boolean flag = false;
        for (int i = 0; i < n; i++) {
            if (x[i] % 2 == 0) {
                System.out.println(x[i]);
                flag = true;
            }
        }
            if (!flag)
                System.out.println(-1);

    }
}
