package fifthweekpractice;

import java.util.Scanner;

/*
На вход подается число Н - длина массива
затем подается массив строк длины Н
после этого число М
Сохранить в другом массиве только те элементы, длина строки которых не превышает М
 */
public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] a = new String[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.next();
        }
        int m = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            if (a[i].length() <= m)
                System.out.print(a[i] + " ");
/*
вариант решения с сохранения в другом массиве
            int k = 0;
            String [] res = new String[n];
            for (String s: a) {
                if (s.length() <= m) {
                    res[k++] = s;
                }
            }
            for (int j = 0; j < k; j++) {
                System.out.println(res[j] + " ");
            }

 */
        }
    }
}
