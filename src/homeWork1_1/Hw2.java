package homeWork1_1;

import java.util.Scanner;

public class Hw2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("a1 = ");
        int a1 = scanner.nextInt();
        System.out.print("b1 = ");
        int b1 = scanner.nextInt();

        double c1 = Math.sqrt((a1 * a1 + b1 * b1) / 2);
        System.out.println("c1 = " + c1);

        System.out.print("a2 = ");
        int a2 = scanner.nextInt();
        System.out.print("b2 = ");
        int b2 = scanner.nextInt();

        double c2 = Math.sqrt((Math.pow(a2,2) + Math.pow(b2,2)) / 2);
        System.out.print("c2 = " + c2);

    }
}
