package homeWork1_1;

import java.util.Scanner;

public class Hm6 {
    public static void main(String[] args) {
        final double ML_PER_KM = 1.60934;
        Scanner scanner = new Scanner(System.in);
        System.out.print("KM = ");
        int count1 = scanner.nextInt();

        double ml1 = count1 / ML_PER_KM;
        System.out.println("ML = " + ml1);

        System.out.print("KM = ");
        int count2 = scanner.nextInt();

        double ml2 = count2 / ML_PER_KM;
        System.out.println("ML = " + ml2);
    }
}
