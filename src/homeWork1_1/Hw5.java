package homeWork1_1;

import java.util.Scanner;

public class Hw5 {
    public static void main(String[] args) {
        final  double DM_PER_CM = 2.54;
        Scanner scanner = new Scanner(System.in);
        System.out.print("DM1 = ");
        int dm1 = scanner.nextInt();

        double cm1 = dm1* DM_PER_CM;
        System.out.println("CM = " + cm1);

        System.out.print("DM = ");
        int dm2 = scanner.nextInt();

        double cm2 = dm2 * DM_PER_CM;
        System.out.println("CM2 = " + cm2);
    }
}
