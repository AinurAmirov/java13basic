package homeWork1_1;

import java.util.Scanner;

public class Hw1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("r1 = ");
        int r1 = scanner.nextInt();
        double v1 = 4.0 / 3 * Math.PI * Math.pow(r1,3);

        System.out.println("V = " + v1);

        System.out.print("r2 = ");
        int r2 = scanner.nextInt();
        double v2 = 4.0 / 3 * Math.PI * Math.pow(r2,3);

        System.out.println("V = " + v2);
        }

    }
