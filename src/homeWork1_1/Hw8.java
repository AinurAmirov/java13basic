package homeWork1_1;

import java.util.Scanner;

public class Hw8 {
    public static void main(String[] args) {
        final int DAY = 30;
        Scanner scanner = new Scanner(System.in);
        System.out.print("many1 = ");
        double many1 = scanner.nextDouble();
        double manyForDay1 = many1 / DAY;
        System.out.println("many for day = " + manyForDay1);

        System.out.print("many = ");
        double   many2 = scanner.nextDouble();
        double manyForDay2 = many2 / DAY;
        System.out.println("many for day = " + manyForDay2);
    }
}
