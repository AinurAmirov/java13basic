package homeWork1_1;

import java.util.Scanner;

public class Hw4 {
    public static void main(String[] args) {
        final int SEC_PER_MIN = 60, MIN_PER_HOUR = 60;
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();

        int hour = count / SEC_PER_MIN / MIN_PER_HOUR % MIN_PER_HOUR;
        int min = count / SEC_PER_MIN % SEC_PER_MIN;

        System.out.println(hour + " " + min);
    }
}
