package homeWork2_2;

import java.util.Scanner;

public class hw3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String [][] m = new String[n][n];
        int k1 = scanner.nextInt();
        int k2 = scanner.nextInt();
        m[k2][k1] = "K";
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (m[i][j] != m[k2][k1]) {
                    if (i == k2 - 2 && j == k1 - 1)
                        m[i][j] = "X";
                    else if (i == k2 - 1 && j == k1 - 2)
                        m[i][j] = "X";
                    else if (i == k2 - 2 && j == k1 + 1)
                        m[i][j] = "X";
                    else if (i == k2 - 1 && j == k1 + 2)
                        m[i][j] = "X";
                    else if (i == k2 + 2 && j == k1 - 1)
                        m[i][j] = "X";
                    else if (i == k2 + 1 && j == k1 - 2)
                        m[i][j] = "X";
                    else if (i == k2 + 2 && j == k1 + 1)
                        m[i][j] = "X";
                    else if (i == k2 + 1 && j == k1 + 2)
                        m[i][j] = "X";
                    else
                        m[i][j] = "0";
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if(j < n - 1) {
                    System.out.print(m[i][j] + " ");
                } else if (j == n - 1)
                    System.out.print(m[i][j]);
            }
            System.out.println();
        }
    }
}
