package homeWork2_2;

import java.util.Scanner;
import java.util.concurrent.DelayQueue;

public class hw6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int d = scanner.nextInt();
        int [][] m = new int[7][4];
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                m[i][j] = scanner.nextInt();
            }
        }
        int sum1 = 0;
        for (int i = 0; i < 7; i++) {
            sum1 += m[i][0];
        }
        int sum2 = 0;
        for (int i = 0; i < 7; i++) {
            sum2 += m[i][1];
        }
        int sum3 = 0;
        for (int i = 0; i < 7; i++) {
            sum3 += m[i][2];
        }
        int sum4 = 0;
        for (int i = 0; i < 7; i++) {
            sum4 += m[i][3];
        }
        if (sum1 <= a && sum2 <= b && sum3 <= c && sum4 <= d)
            System.out.println("Отлично");
        else
            System.out.println("Нужно есть поменьше");
    }
}
