package homeWork2_2;

import java.util.Scanner;

public class hw5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int [][] m = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                m[i][j] = scanner.nextInt();
            }
        }
        int sum = 0;
        int l = n - 1;
        for (int i = 0; i < n; i++, l--){
            int k = n - 1;
            for(int j = 0; j < n; j++, k--){
                if (m[i][j] == m[k][l]) {
                    sum += 1;
                }
            }
        }
        if (sum == n * n)
            System.out.println(true);
        else
            System.out.println(false);
    }
}
