package homeWork2_1;

import java.util.Arrays;
import java.util.Scanner;

public class hw7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        int[] b = new int[n];
        for (int i = 0; i < n; i++) {
            b[i] = (int) Math.pow(a[i], 2);
        }
        Arrays.sort(b);
        for (int i = 0; i < n; i++) {
            System.out.print(b[i] + " ");
        }
    }
}
