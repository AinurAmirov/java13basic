package homeWork2_1;

import java.util.Arrays;
import java.util.Scanner;

public class hw8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        Arrays.sort(a);
        int m = scanner.nextInt();
        int sum = 0;
        int b = Math.abs(a[0] - m);
        for (int i = 1; i < n; i++) {
            if (b >= Math.abs(a[i] - m)) {
                sum += 1;
                b = Math.abs(a[sum] - m);
            }
        }
        System.out.println(a[sum]);
    }
}
