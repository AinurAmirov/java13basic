package homeWork2_1;

import java.util.Scanner;

public class hw6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.next();
        String [] a = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.",
                "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            int b = (char)ch - 'А';
            System.out.print(a[b] + " ");
        }
    }
}
