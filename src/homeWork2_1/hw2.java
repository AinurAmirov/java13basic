package homeWork2_1;

import java.util.Scanner;

public class hw2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        int[] b = new int[m];
        for (int i = 0; i < m; i++) {
            b[i] = scanner.nextInt();
        }
        boolean flag = true;
        if (n == m) {
            for (int i = 0; i < n; i++) {
                if (a[i] != b[i]) {
                    flag = false;
                    break;
                }
            }
        } else {
            flag = false;
        }
        System.out.println(flag);
    }
}
