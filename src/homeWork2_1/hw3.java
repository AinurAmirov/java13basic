package homeWork2_1;

import java.util.Arrays;
import java.util.Scanner;

public class hw3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        int x = scanner.nextInt();
        int[] b = new int[n + 1];
        System.arraycopy(a, 0, b, 0, n);
        b[n] = x;
        for (int i = n; i > -1; i--) {
            if (b[n] < b[i - 1]) {
                continue;
            } else {
                System.out.println(i);
                break;
            }
        }
    }
}
