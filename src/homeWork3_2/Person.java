package homeWork3_2;

import java.util.Objects;

public class Person {
    private String namePerson;
    private int id;
    private static int count = 1;
//    private String bookForPerson;
    private String nameBook = "noBook";
    private String nameAutorBook = "noBook";
    private int idBook = 0;

//    public Person(String namePerson, int id) {

//    public Person(){
//        this.id = count;
//        count++;
//        this.id = id;
//    }
    public Person(String namePerson) {
        this.namePerson = namePerson;
        this.id = count;
        count++;
//        this.id = id;

//        setId(getId() + 1);
//        this.id = id;
    }

    public String getNamePerson() {
        return namePerson;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameBook() {
        return nameBook;
    }

    public String getNameAutorBook() {
        return nameAutorBook;
    }

    public int getIdBook() {
        return idBook;
    }

//    public String getBookForPerson() {
//        return bookForPerson;
//    }

    public void setNameBook(String nameBook) {
        this.nameBook = nameBook;
    }

    public void setNameAutorBook(String nameAutorBook) {
        this.nameAutorBook = nameAutorBook;
    }

    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }

    public void setBookForPerson(Book book){
        this.nameBook = book.getNameBook();
        this.nameAutorBook = book.getNameAuthor();
        this.idBook = book.getId();
//        this.bookForPerson = nameBook + " " + nameAutorBook + " " + idBook;
    }

    @Override
    public String toString(){
        return namePerson /*+ " " + nameBook + " " + nameAutorBook + " " + idBook */; /*+ " " + id*/
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return namePerson.equals(person.namePerson);
    }

    @Override
    public int hashCode() {
        return Objects.hash(namePerson);
    }
}
