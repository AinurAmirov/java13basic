package homeWork3_2;

import javax.crypto.spec.PSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Bibliography {
    public static Scanner input = new Scanner(System.in);
    private static List<Book> books = new ArrayList<>();
    private static List<Person> personList = new ArrayList<>();

    public static void main(String[] args) {
        Bibliography menu = new Bibliography();
        Scanner scanner = new Scanner(System.in);

        books.add(new Book("Book1", "Author1"));
        books.add(new Book("Book2", "Author2"));
        books.add(new Book("Book3", "Author3"));

        personList.add(new Person("person1"));
        personList.add(new Person("person2"));
        personList.add(new Person("person3"));

        while (true) {
            int choice = menu.getMenu();
            if (choice == 1) {
                System.out.println("Введите название и автора книги: ");
                String a = scanner.nextLine();
                String b = scanner.nextLine();
                Book book = new Book(a, b);
                if (!menu.checkBook(a)) {
                    menu.addBookInBooksList(book);
                } else
                    System.out.println(" и не может быть добавлена");
            } else if (choice == 2) {
                System.out.println("Введите имя посетителя: ");
                String personName = scanner.nextLine();
                Person person = new Person(personName);
                menu.addPersonInPersonList(person);
            } else if (choice == 3) {
                System.out.println("Введите название книги и имя автора, которую хотите передать посетителю: ");
                String bookName = scanner.nextLine();
                String bookAuthor = scanner.nextLine();
                menu.sellBook(bookName, bookAuthor);
            } else if (choice == 4) {
                System.out.println("Введите название книги, которую хотите вернуть: ");
                String bookNameBack = scanner.nextLine();
                menu.bookGetBack(bookNameBack);
            } else if (choice == 5) {
                System.out.println("Введите имя посетителя: ");
                String personNameCheck = scanner.nextLine();
                menu.checkPerson(personNameCheck);
            } else if (choice == 6) {
                System.out.println("Введите название книги: ");
                String bookCheck = scanner.nextLine();
                menu.checkBook(bookCheck);
            } else if (choice == 7) {
                for (Book book1 : books)
                    System.out.println(book1);
            } else if (choice == 8) {
                for (Person person1 : personList)
                    System.out.println(person1);
            } else if (choice == 0)
                break;
        }
    }

        public int getMenu() {
            int choice;
            while (true) {
                System.out.println("\nОсновное меню:");
                System.out.println("1: добавить книгу");
                System.out.println("2: добавить посетителя");
                System.out.println("3: забрать книгу");
                System.out.println("4: вернуть книгу");
                System.out.println("5: проверить есть ли у посетителя книга");
                System.out.println("6: проверить у какого посетителя находится книга");
                System.out.println("7: вывести доступные книги");
                System.out.println("8: показать список посетителей");
                System.out.println("0: закрыть меню");
                choice = input.nextInt();
                if (choice < 0 || choice > 8) {
                    System.out.println("Введен неверный пункт меню");
                } else
                    break;
            }
            return choice;
        }

    public boolean addBookInBooksList (Book book) {
        if (!book.getNameBook().isEmpty() && !book.getNameAuthor().isEmpty()) {
            if (!books.contains(book)) {
                books.add(book);
                System.out.println(" книга добавлена");
                return true;
            } else {
                System.out.println("Книга " + book + "не добавлена, она уже есть в списке книг.");
                return false;
            }
        } else {
            System.out.println("Данные книги введены не корректно");
            return false;
        }
    }

    public boolean addPersonInPersonList (Person person) {
        if (!person.getNamePerson().isEmpty()) {
            if (!personList.contains(person)) {
                personList.add(person);
                return true;
            } else {
                System.out.println("Посетитель " + person + "не добавлен, он уже есть в списке посетителей.");
                return false;
            }
        } else {
            System.out.println("Данные посетителя введены не корректно");
            return false;
        }
    }

    public void sellBook (String a, String b) {
        Book book = new Book(a, b);
        boolean flag = false;
        for (Book book1 : books) {
            if (book1.equals(book)) {
                flag = true;
                book.setId(book1.getId());
                System.out.println("Введите имя человека которому хотите передать книгу: ");
                input.nextLine();
                String c = input.nextLine();
                Person person = new Person(c);
                for (Person person1 : personList
                ) {
                    if (person1.equals(person)) {
                        person1.setBookForPerson(book);
                        book1.setNameBook("bookForPerson");
                        book1.setNameAuthor("bookForPerson");
                    }
                }
            }
        }
        if (!flag)
            System.out.println("данной книги нет в библиотеке!");
    }

    public void bookGetBack(String nameBook){
        for (Person person1 : personList) {
            if (person1.getNameBook().equals(nameBook)) {
                for (Book book1 : books) {
                    if (person1.getIdBook() == book1.getId()){
                        book1.setNameBook(person1.getNameBook());
                        book1.setNameAuthor(person1.getNameAutorBook());
                        person1.setNameBook("noBook");
                        person1.setNameAutorBook("noBook");
                        person1.setIdBook(0);
                        System.out.println("Введите оценку книги по шкале от 0 до 5 баллов: ");
                        int n = input.nextInt();
                        while (n < 0 && n > 5) {
                            System.out.println("Введена неверная цифра, введите от 1 до 5!");
                            n = input.nextInt();
                        }
                        System.out.println("оценка учтена!");
                        book1.setGrade(n);
                    }
                }
            }
        }
    }

    public void checkPerson(String a) {
        boolean flag = false;
        for (Person person1 : personList) {
            if (person1.getNamePerson().equals(a)) {
                flag = true;
                if (!person1.getNameBook().equals("noBook")) {
                    System.out.println("У посетителя " + person1.getNamePerson() + " есть книга: " + person1.getNameBook() + " "
                            + person1.getNameAutorBook());
                } else {
                    System.out.println("У посетителя " + person1.getNamePerson() + " нет книг");
                }
            }
        } if (!flag)
            System.out.println("Неверно введено имя посетителя!");
    }

    public boolean checkBook(String a) {
        boolean flag = false;
        for (Person person1 : personList) {
            if (person1.getNameBook().equals(a)) {
                flag = true;
                System.out.print("Книга " + person1.getNameBook() + " " + person1.getNameAutorBook() + " находится у "
                        + person1.getNamePerson());
            }
        } if (!flag)
            System.out.print("Данной книги нет у посетителей!");
        return flag;
    }
}
