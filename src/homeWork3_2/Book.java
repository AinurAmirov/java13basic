package homeWork3_2;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class Book {
    private String nameAuthor;
    private String nameBook;
    private static int count = 1;
    private int id;
    private static int grade;
    private static int countGrade = 0;

    Scanner scanner = new Scanner(System.in);


    public Book(String nameBook, String nameAuthor) {
        this.nameAuthor = nameAuthor;
        this.nameBook = nameBook;
        this.id = count;
        count++;
    }

    public String getNameAuthor() {
        return nameAuthor;
    }

    public String getNameBook() {
        return nameBook;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNameAuthor(String nameAuthor) {
        this.nameAuthor = nameAuthor;
    }

    public void setNameBook(String nameBook) {
        this.nameBook = nameBook;
    }

    public void setGrade(int grade) {
        this.grade += grade;
        countGrade++;
        System.out.println("средняя оценка книги равна: " + this.grade / countGrade);
    }

    public int getGrade() {
        return grade / countGrade;
    }

    @Override
    public String toString(){
        return nameBook + " " + nameAuthor /*+ " " + id*/;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return nameAuthor.equals(book.nameAuthor) && nameBook.equals(book.nameBook);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameAuthor, nameBook);
    }
}
